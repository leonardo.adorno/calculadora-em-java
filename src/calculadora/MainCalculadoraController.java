/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class MainCalculadoraController implements Initializable {
    
    @FXML
    private Label Lcelsius;
    
    @FXML
    private Label Lkelvin;
    
    @FXML
    private Label Lfarenheit;
   
    
    @FXML
    private TextField field;
    
    @FXML
    private void Celsius(ActionEvent event) {
       double valor = Double.parseDouble(field.getText());
       double resK = valor+273;
       double resF = valor*1.8+32;
       double resC = valor;
       Lkelvin.setText(" " + resK);
       Lfarenheit.setText(" " + resF);
       Lcelsius.setText(" " + valor);
       
    }
    
     @FXML
    private void Kelvin(ActionEvent event) {
       double valor = Double.parseDouble(field.getText());
       double resK = valor;
       double resC = valor-273;
       double resF = resC*18/10+32;
       
       Lkelvin.setText(" " + valor);
       Lfarenheit.setText(" " + resF);
       Lcelsius.setText(" " + resC);
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
